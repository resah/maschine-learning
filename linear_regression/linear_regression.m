
% Initialization
clear ; close all; clc


%%%%%%%%%%%%%%%%%%%%%%%%%
% read data files

iterations = 10000;

x = load('data/features.dat');
y = load('data/results.dat');

theta0 = 1
theta1 = 2

fprintf('Starting. Press enter to continue.\n');
pause;



%%%%%%%%%%%%%%%%%%%%%%%%%
% starting

J = calculateCost(x, y, theta0, theta1)

for k = 1:iterations
	[theta0, theta1] = gradientDecrease(x, y, theta0, theta1);
	J = calculateCost(x, y, theta0, theta1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%
% output

J
theta0
theta1





