%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate cost

function J = calculateCost(x, y, theta0, theta1)
	m = length(x);
	p = 0;
	for i = 1:m
		h = theta0 + theta1 * x(i);
		p = p + (h - y(i)) ^ 2;
	end

	J = 1 / (2 * m) * p;
end
