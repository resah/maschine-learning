%%%%%%%%%%%%%%%%%%%%%%%%%
% gradient decrease

function [theta0, theta1] = gradientDecrease(x, y, theta0, theta1)
	alpha = 0.03;
	m = length(x);

	q0 = 0;
	q1 = 0;
	for i = 1:m
		h = theta0 + theta1 * x(i);
		q0 = q0 + (h - y(i)) * 1;
		q1 = q1 + (h - y(i)) * x(i);
	end

	prediction0 = theta0 - alpha * (1 / m) * q0;
	prediction1 = theta1 - alpha * (1 / m) * q1;

	theta0 = prediction0;
	theta1 = prediction1;
end
